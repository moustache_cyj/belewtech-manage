/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-19 14:23:14
 * @LastEditTime: 2021-11-22 11:07:16
 * @Description: file context
 */
const {FailResModel} = require('../utils/resModel')
const {loggerError} = require('../utils/log4js')
const handleError = async(ctx,next) =>{
    try {
       await next()
    } catch (error) {
        if(error.code){
            return ctx.body = new FailResModel(error.code)
        }else{
            loggerError().error(error || error.message)
            return ctx.body = new FailResModel("40001")

        }
    }
}
module.exports = handleError