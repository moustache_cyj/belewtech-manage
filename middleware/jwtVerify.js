/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-19 14:13:05
 * @LastEditTime: 2021-11-26 11:10:01
 * @Description: file context
 */
const {httpError} = require('../utils/resModel')
const {tokenVerify} = require('../utils/jwt')
const jwtVerfiy = async(ctx,next) =>{
    if(!ctx.request.headers.authorization){
        throw new httpError("40002")
    }else{
        ctx.user = tokenVerify(ctx.request.headers.authorization)
        await next()
    }
}
module.exports = jwtVerfiy