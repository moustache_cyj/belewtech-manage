/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 10:46:22
 * @LastEditTime: 2022-02-01 13:26:22
 * @Description: file context
 */
const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const koaBody = require('koa-body')
const logger = require('koa-logger')
const {loggerError,loggerInfo} = require('./utils/log4js')
const users = require('./routes/users')
const menu = require('./routes/menu')
const role = require('./routes/role')
const util = require('./routes/utils')
const cors = require('koa2-cors')
const handleErr = require('./middleware/handleErr')

require('./config/db')
// error handler
onerror(app)

// middlewares error
app.use(handleErr)

// 跨域
app.use(cors({origin:'*'}))
app.use(bodyparser({
  enableTypes:['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))
app.use(require('koa-static')(__dirname + '/upload'))
app.use(views(__dirname + '/views', {
  extension: 'pug'
}))
// logger
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  loggerInfo().info(`${ctx.method} ${ctx.url} - ${ms}ms  ${JSON.stringify(ctx.request.params || ctx.request.body)}`)
})

// app.use(koaBody({
//   multipart:true,
//   formidable:{
//     keepExtensions:true
//   }
// }))
// routes
app.use(users.routes(), users.allowedMethods())
app.use(menu.routes(),menu.allowedMethods())
app.use(role.routes(),role.allowedMethods())
app.use(util.routes(),util.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app
