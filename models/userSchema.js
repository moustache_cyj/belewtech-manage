/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-16 15:47:39
 * @LastEditTime: 2021-11-29 14:05:32
 * @Description: file context
 */
/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-16 15:47:39
 * @LastEditTime: 2021-11-23 09:34:31
 * @Description: user model
 */
const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    // 用户id
    userId:Number, 
    // 用户昵称
    userName:String,
    // 用户密码 md5加密
    userPwd:String,
    // 用户邮箱
    userEmail:String,
    // 手机号码
    mobile:String,
    // 0:男 1:女
    sex:Number,
    // 岗位
    job:String,
    // 状态 1.true 删除 2.未删除
    deleted:{
        type:Boolean,
        default:false
    },
    // 用户角色 0：系统管理员 1：普通管理员
    role:{
        type:Number,
        default:1
    },
    // 系统角色
    roleList:[],
    // 创建时间
    createTime:{
        type:Date,
        default:Date.now()
    },
    // 最后一次登陆时间
    lastLoginTime:{
        type:Date,
        default:Date.now()
    },
    remark:String
})

module.exports = mongoose.model('users',userSchema)