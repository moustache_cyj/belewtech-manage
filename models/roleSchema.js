/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-25 10:05:21
 * @LastEditTime: 2021-11-25 15:52:29
 * @Description: file context
 */
const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
    roleName:String,
    remark:String,
    permissionList:{
        checkedKeys:[],
        halfCheckedKeys:[],
    },
    updateTime:{
        type:Date,
        default: Date.now()
    },
    createTime: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('roles',roleSchema)