const mongoose = require('mongoose') 

const menuSchema = new mongoose.Schema({
     menuType:Number,
     menuName:String,
     menuCode:String,
     path:String,//路由地址
     icon:String,
     component:String,
     // 1 启用 2 禁用
     menuState:{
         type:Number,
         default:1
     },
     parentId:[mongoose.Types.ObjectId],
     createTime:{
         type:Date,
         default: Date.now()
     }, 
     updateTime:{
        type: Date,
        default: Date.now()
    },
})
module.exports = mongoose.model('menus', menuSchema)