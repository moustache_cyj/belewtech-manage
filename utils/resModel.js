/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 16:18:28
 * @LastEditTime: 2021-11-18 15:41:10
 * @Description: file context
 */
const STATUS_CODE  = require('../config/statusCode.js')

class ResModel {
    constructor(code,data){
        this.code = code
        this.msg = STATUS_CODE[code]
        this.data = data || null
    }
}
class SuccResModel extends ResModel{
    constructor(code,data){
        super(code,data)
        this.success = true
    }
}
class FailResModel extends ResModel{
    constructor(code){
        super(code)
        this.success = false
    }
}
class httpError extends Error {
    constructor(code,error){
        super()
        this.code = code
        this.error = error
        this.message = STATUS_CODE[code]
    }
}
module.exports = {
    FailResModel,
    SuccResModel,
    httpError
}