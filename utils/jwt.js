/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-19 13:35:05
 * @LastEditTime: 2021-11-26 11:00:31
 * @Description: jwt 跨域通讯方案
 */
const Jwt = require('jsonwebtoken')
const {SECRET} = require('../config/common')
const token = (payload) => Jwt.sign(payload,SECRET,{expiresIn:'1h'})
const tokenVerify = (token) => Jwt.verify(token,SECRET)
module.exports = {
    token,
    tokenVerify
}