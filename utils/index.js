/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-22 13:58:35
 * @LastEditTime: 2021-11-26 16:29:34
 * @Description: 工具函数
 */
const Role = require('../models/roleSchema')
const Menu = require('../models/menuSchema')
function getMenuTree(rootTree, menuList){
    rootTree.map(f => {
         f.children  = []
         menuList.map(s => {
             const lastParentId = s.parentId.slice(-1)[0]
             if(lastParentId === f._id){
                  f.children.push(s)
             }
         })
        if(f.children.length > 0){
            getMenuTree(f.children,menuList)
        }
     })
     return rootTree
}

const  getPermissionListByRole = async (user,roleList) =>  {
    let permissionList = []
    let menuList = []
    console.log(user)

    if(user.role === 0){
       menuList = await Menu.find({}) || []
    }else{
       let roles = await Role.find({_id:{$in:roleList}})
       roles.map(r => {
          const {checkedKeys,halfCheckedKeys} = r.permissionList
          permissionList = permissionList.concat([...checkedKeys,...halfCheckedKeys])
       })
       permissionList = [...new Set(permissionList)] 
       menuList = await Menu.find({_id:{$in:permissionList}})
    }
    console.log(menuList)
    return menuList
}
const getAction = (menuList) => {
     const a_l = menuList.filter(m => m.menuType === 2)
     const action = []
     a_l.map(v => {
         action.push(v.menuCode)
     })
     return action
}

module.exports = {
    pageUtil:(page,pageSize) => {return {limit:pageSize * 1,skip:(page * 1 - 1)*pageSize}},
    getMenuTree:getMenuTree,
    getPermissionListByRole:getPermissionListByRole,
    getAction:getAction,
    getMenuTreeRoot:(menuList)=> menuList.filter(menu =>  menu.parentId.length === 0 )

}