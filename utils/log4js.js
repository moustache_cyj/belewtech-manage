/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 11:23:32
 * @LastEditTime: 2021-11-15 14:31:08
 * @Description: file context
 */
const log4js = require('log4js')
const path = require('path')
const levels = {
    'trace':log4js.levels.TRACE,
    'debug':log4js.levels.DEBUG,
    'info':log4js.levels.INFO,
    'warn':log4js.levels.WARN,
    'error':log4js.levels.ERROR,
    'fatal':log4js.levels.FATAL,
}

log4js.configure({
    appenders:{
        console:{type:'console'},
        error:{
            type:"dateFile",
            pattern:"-yyyy-MM-dd.log",
            encoding:"utf-8",
            filename:path.join('.','logs/error',"error.log"),
            alwaysIncludePattern: true,
        },
        info:{
             type:"dateFile",
             pattern:"-yyyy-MM-dd.log",
             encoding:"utf-8",
             filename:path.join('.','logs/info',"info.log"),
             alwaysIncludePattern: true, 
        }
    },
    categories:{
        default:{appenders:['console'],level:'debug'},
        error:{
            appenders:['console','error'],
            level:'error'
        },
        info:{appenders:['console','info'],level:'info'}
    }
})

module.exports = {
    // info logger
    loggerInfo:()=> log4js.getLogger('info'),
    // error logger
    loggerError:()=>log4js.getLogger('error')
}