/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-16 15:29:32
 * @LastEditTime: 2021-11-22 13:47:50
 * @Description: mongdb connect
 */
const {URL} = require('./common')
const {loggerError,loggerInfo} = require('../utils/log4js')
const mongoose = require('mongoose')

mongoose.connect(URL);

var db = mongoose.connection;
db.on('error', (err)=>{loggerError().error(err)});
db.once('open', function() {
    loggerInfo().info('mongdb connect!!')
});