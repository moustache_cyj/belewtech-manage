/*
 * @Author: Moustach_壮壮
 * @Date: 2021-12-08 16:54:45
 * @LastEditTime: 2021-12-08 17:20:14
 * @Description: file context
 */
const router = require('koa-router')()
const fs = require('fs')
const path = require('path')
const {SuccResModel} = require('../utils/resModel')
router.prefix('/api/util')

router.post('/upload', async(ctx,next) => {
    try {
        console.log(ctx.request.files.file)
        const file = ctx.request.files.file
        const fileName = file.name.split('.')[0] + new Date().getTime() + '.' + file.name.split('.')[1]
        const reader = fs.createReadStream(file.path)
        const writer = fs.createWriteStream(path.join(__dirname,'../upload/') + `${fileName}`)
        reader.pipe(writer)
        return ctx.body = new SuccResModel('20014',fileName)
    } catch (error) {
        throw error
    }
})
module.exports = router