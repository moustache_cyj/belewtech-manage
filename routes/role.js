/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-25 10:08:58
 * @LastEditTime: 2021-11-26 13:39:06
 * @Description: file context
 */
const router = require('koa-router')()
const {SuccResModel,FailResModel,httpError} = require('../utils/resModel')
const Role = require('../models/roleSchema')

router.prefix('/api/role')

router.get('/list', async(ctx) => {
      try {
          const {roleName,_id} = ctx.request.query
          const params = {}
          if(roleName) params.roleName = roleName
          if(_id) params._id  = _id
          const roleList = await Role.find(params)
          return ctx.body = roleList ? new SuccResModel("20008",roleList) : new FailResModel("50012")
      } catch (error) {
          throw error
      }
})
router.get('/detail',async(ctx) => {
    try {
        const {_id} = ctx.request.query
        if(!_id) return ctx.body = new FailResModel("50005")  
        const role = await Role.findOne({_id})
        return ctx.body =  role ? new SuccResModel("20010",role) : new FailResModel("50015")
    } catch (error) {
        throw error
    }
})
router.post('/add', async(ctx) => {
    try {
        const {roleName} = ctx.request.body
        if(!roleName) return new FailResModel('50013')
        const role = await Role.create({...ctx.request.body})
        return ctx.body = role? new SuccResModel("20009") : new FailResModel("50014")
    } catch (error) {
        throw error
    }
})

router.put('/permission/update', async (ctx) => {
    const { _id, permissionList } = ctx.request.body;
    try {
      let params = { permissionList, updateTime: new Date() }
      let res = await Role.findByIdAndUpdate({_id}, params)
      return ctx.body = res? new SuccResModel("20011") : new FailResModel("50016")
    } catch (error) {
      throw error
    }
  })
  router.delete('/delete', async (ctx) => {
    try {
        const {roleId} = ctx.request.body
        if(!roleId){
            return ctx.body = new FailResModel("500017")
        }
        const role = await Role.findByIdAndRemove({_id:roleId})
        return ctx.body = role ? new SuccResModel('200012') :new FailResModel('50018')
    } catch (error) {
        throw error
    }
 })
module.exports = router