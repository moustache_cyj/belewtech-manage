/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-15 10:46:22
 * @LastEditTime: 2021-12-07 14:09:34
 * @Description: 用户管理模块
 */
const router = require('koa-router')()
const {SuccResModel,FailResModel,httpError} = require('../utils/resModel')
const {token,tokenVerify} = require('../utils/jwt')
const User = require('../models/userSchema')
const Counter = require('../models/counterSchema')
const Menu = require('../models/menuSchema')
const jwtVerfiy = require('../middleware/jwtVerify')
const {pageUtil,getPermissionListByRole,getMenuTree,getMenuTreeRoot,getAction} = require('../utils/index')
const MD5  = require('md5')

router.prefix('/api/user')

router.post('/admin',async(ctx) => {
   try {
       const {userName,userPwd} = ctx.request.body
       if(!userName || !userPwd){
         return ctx.body = new FailResModel("50002")
       }
       const total = await Counter.countDocuments()
       console.log(total)
       //初始化counter表
       if(total == 0){
           await Counter.create({_id:'userId',sequence_value:10000})
       }
       const doc  = await Counter.findByIdAndUpdate({_id:'userId'},{$inc:{sequence_value:1}},{new:true})
       const user  = await User.create({
         userId:doc.sequence_value,
         userName,
         userPwd:MD5(userPwd),
         role:0
       })
       const system = await Menu.create({
         menuType:1,
         menuName:"系统管理",
         icon:"system",
         path:"/manage",
         component:"SystemManage"
       })
       const parentId = system._id
       const menu = await Menu.create({
        menuType:1,
        menuName:"菜单管理",
        icon:"menu",
        path:"/manage/menu",
        parentId,
        component:"MenuManage"
      })
      if(system && menu && user){
        return ctx.body = new SuccResModel('20000')
      }else{
        return ctx.body = new FailResModel("50000")
      }
   } catch (error) {
       throw error
   }
})

router.post('/login', async (ctx) => {
    try {
      const {userName,userPwd} = ctx.request.body;
      if(!userName || !userPwd){
        return ctx.body = new FailResModel("50002")
      }
      const res = await User.findOne({
        userName,
        userPwd:MD5(userPwd)
      })
      if(res){
        const auth = token(ctx.request.body)
        return ctx.body = new SuccResModel('20001',{auth,...res._doc})
      }else{
        return ctx.body = new FailResModel("50001")
      }
    } catch (error) {
      throw error
    }
})
router.get('/list',async(ctx) => {
    try {
       const {page,pageSize,userId,userName} = ctx.request.query
       if(!page || !pageSize){
         return ctx.body = new FailResModel("40003")
       }
       const {skip,limit} = pageUtil(page,pageSize)
       let params = {}
       if(userId) params.userId = userId
       if(userName) params.userName = userName
       const users = await User.find({...params,deleted:false},{_id:0,userPwd:0,__v:0},{limit,skip}).exec()
       const total = await User.countDocuments(params)
       if(users && total){
        const data = {list:users,total}
        return ctx.body = new SuccResModel('2004',data)
      }else{
        return ctx.body = new FailResModel("5007")
      }
    } catch (error) {
        throw error
    }
})

router.post('/add',async(ctx) => {
    try {
      const {userName,userEmail,mobile,job} = ctx.request.body
      if(!userName || !userEmail || !mobile){
        return ctx.body  = new FailResModel('50002')
    }else{
        const doc  = await  Counter.findByIdAndUpdate({_id:'userId'},{$inc:{sequence_value:1}},{new:true})
        const user = await User.findOne({$or:[{userName},{userEmail}],deleted:false},'_id userName userEmail')
        if(user){
           return ctx.body = new FailResModel('50003')
        }else{
           const res = await User.create({userId:doc.sequence_value,userEmail,mobile,job,userName,role:1,userPwd:MD5('123456'),deleted:false})
           return res ?  ctx.body = new SuccResModel('20002') : ctx.body = new FailResModel('50004')
        }
    }
    } catch (error) {
       throw error
    }
})

router.put('/update',async(ctx)=>{
   try {
        const {userId,userName,userEmail,job,mobile,roleList} = ctx.request.body
        if(!userId){
           return ctx.body  = new FailResModel('50005') 
        }
        const user = await User.findOneAndUpdate({userId},{userName,userEmail,job,mobile,roleList},{new:true})
        if(user){
             return ctx.body = new SuccResModel("20003",user)
        }else{
             return ctx.body = new FailResModel("50006")
        }
   } catch (error) {
        throw error
   }
})
router.delete('/delete',async(ctx) => {
  try {
    const { userId } = ctx.request.body
    if(!userId){
      return ctx.body = new FailResModel("50005")
    }
    const user = await User.updateOne({userId},{deleted:true})
    if(user){
      return ctx.body = new SuccResModel("20003")
    }else{
      return ctx.bdoy = new FailResModel("50006")
    }
  } catch (error) {
    throw error
  }
})

router.get('/getPermissionList',jwtVerfiy, async(ctx) => {
  try {
     const {userName} = ctx.user
     const user = await User.findOne({userName})
     const menuList =  await getPermissionListByRole(user._doc,user.roleList)
     //  moogoosedb 的值无法直接修改转换一下
     const menuListJson = JSON.parse(JSON.stringify(menuList))

     const rootTree = getMenuTreeRoot(menuListJson)
     const tree = getMenuTree(rootTree,menuListJson)
     const action = getAction(menuListJson)
     const permission = {}
     // 左侧导航栏的树
     permission.tree = tree
     // 可以点击的按钮action
     permission.action = action
     // 有权限的菜单列表
     permission.menu = menuList

     return ctx.body = new SuccResModel("20013",permission)
  } catch (error) {
     throw  error
  }
})

module.exports = router
