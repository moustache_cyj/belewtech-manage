/*
 * @Author: Moustach_壮壮
 * @Date: 2021-11-18 15:44:40
 * @LastEditTime: 2021-11-26 14:04:33
 * @Description: file context
 */
const router = require('koa-router')()
const {loggerError,loggerOut} = require('../utils/log4js')
const {SuccResModel,FailResModel} = require('../utils/resModel')
const jwtVerfiy = require('../middleware/jwtVerify')
const Menu  = require('../models/menuSchema')
const {getMenuTree,getMenuTreeRoot} = require('../utils/index')

router.prefix('/api/menu')

router.get('/list', async (ctx) => {
    try {
        const {menuName,menuState} = ctx.request.query
        const params = {}
        if(menuName) params.menuName = menuName
        if(menuState) params.menuState = menuState
        const menu = await Menu.find(params) || []
        //  moogoosedb 的值无法直接修改转换一下
        const menuList = JSON.parse(JSON.stringify(menu))
        // 获得第一级没有父级的根
        const rootTree = getMenuTreeRoot(menuList)
        const tree = getMenuTree(rootTree,menuList)
        return ctx.body  = new SuccResModel('123',tree)
    } catch (error) {
        throw error
    }
})

router.post('/add', async(ctx) => {
    try {
        const {menuType,menuName} = ctx.request.body
        if(!menuType || !menuName){
            return ctx.body = new FailResModel("50002")
        }
        const menu = await Menu.create({...ctx.request.body})
        if(menu){
            return ctx.body = new SuccResModel("20005")
        }else{
            return ctx.body = new FailResModel("50008")
        }
    } catch (error) {
        throw error
    }
})

router.put('/edit', async(ctx) => {
   try {
       const {_id} = ctx.request.body
        if(!_id){
            return ctx.body = new FailResModel("50009")
        }
        const menu = await Menu.findByIdAndUpdate({_id},{...ctx.request.body})
        if(menu){
            return ctx.body = new SuccResModel("20007")
        }else{
            return ctx.body = new FailResModel("50011")
        }
   } catch (error) {
       throw error
   }
})

router.delete('/delete', async (ctx) => {
    try {
        const {_id} = ctx.request.body
        if(!_id){
            return ctx.body = new FailResModel("50009")
        }
        // 删除本身
        const menu = await Menu.findByIdAndRemove({_id})
        if(menu){
            // 删除子项
            const res = await Menu.deleteMany({parentId:{$all:_id}})
            return ctx.body = res ? new SuccResModel('20006') :new FailResModel('50010')
        }else{
            return ctx.body = new FailResModel("40004")
        }
    } catch (error) {
        throw error
    }
 })

module.exports = router